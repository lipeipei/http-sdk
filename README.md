# 可爱猫.http-sdk

#### 介绍
可爱猫|http-sdk，主要提供了可爱猫http版的sdk全部功能！支持可爱猫4和可爱猫5，欢迎大家使用！欢迎大家提pr，让我们一起完善吧！

#### 软件架构
本软件纯易语言开发！第三方模块使用了精易模块！另外加载了可爱猫官方提供的sdk


#### 安装教程

1.  下载可爱猫！(https://wwa.lanzous.com/b0ew5ncjg 密码：gagm)
2.  解压可爱猫到你喜欢的目录(绿色免安装)，但是记得关闭杀毒或者放行[大概是易语言没买win的证书吧，各个杀毒都杀，但是真不是病毒]
3.  把本插件的发行版本(或者git根目录里的dll注意[http.cat.dll是可爱猫4版本，iHttp.cat.dll是可爱猫5版本])，放进可爱猫目录里的app下面即可！
4.  打开可爱猫！

#### 使用说明

1.  首先你得是一位服务端开发者，会go/php/java/c#/python/c++等任何一种可用作服务端的语言即可！
2.  根据本插件提供的开发文档进行功能开发，文档地址：https://doc.vwzx.com/web/#/6?page_id=123
3.  开发你喜欢的功能吧！如果你是phper，也可以直接看根目录提供的robot.php进行学习参考

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支 并完善相关功能
3.  提交代码
4.  新建 Pull Request 我会进行审核合并！


#### 交流

1.  可爱猫|http-sdk QQ群：①：44520959 ②：154390891
2.  可爱猫|http-sdk 论坛：https://www.ikam.cn/
3.  本仓库底下的评论
4.  本仓库根目录里的i_eras.jpeg微信号，会拉你进微信开发群！
5.  其他自发组织的交流
6.  如果你觉得好，也可用捐赠的方式和俺交流！跪谢！啊哈哈～
<img src="//gitee.com/ikam/http-sdk/raw/master/i_eras.jpeg" width = "200" alt="机器人DEMO" align=center />
<img src="//gitee.com/ikam/http-sdk/raw/master/IMG_3605.JPG" width = "200" alt="捐赠" align=center />
<img src="//gitee.com/ikam/http-sdk/raw/master/IMG_3606.JPG" width = "200" alt="捐赠" align=center />

7.捐赠列表(若未统计在内的辛苦连续群主，跪谢！)：https://www.ikam.cn/thread-19.htm
